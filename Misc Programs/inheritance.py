class Person:
    def __init__(self,fname,lname):
        self.fname=fname
        self.lname=lname

    def printname(self):
        print(self.fname,self.lname)


x=Person("john","Doe")
x.printname()


class Student(Person):
  def __init__(self, fname, lname):
    Person.__init__(self, fname, lname)

y=Student("mike","joe")
y.printname()