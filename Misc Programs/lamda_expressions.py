
#lambda fuction with one argument
'''
x = lambda a : a + 100
print(x(255))
'''

#labda fn with multiple args

x=lambda a,b : a / b
print(x(30,5))