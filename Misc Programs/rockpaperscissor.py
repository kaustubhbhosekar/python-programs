import random

game=['Rock','Paper','Scissor']
computer_win=0
player_win=0
computer=random.choice(game)
no_of_chances=random.randint(1,10)
totalgames=no_of_chances
player=input("Choose one from Rock,Paper or Scissor")
while no_of_chances!=0:
    if computer==player:
        print("Both win")
        computer_win+=1
        player_win+=1
        no_of_chances-=1
    elif((computer=="Rock" and player=="Scissor")or
         (computer=="Scissor" and player=="Paper")or
         (computer=="Paper" and player=="Rock")):
         print("Computer wins")
         computer_win+=1
         no_of_chances-=1
    else:
        print("Player wins")
        player_win+=1
        no_of_chances-=1
    computer=random.choice(game)
    player=input("Choose one from Rock,Paper or Scissor: ")
if(no_of_chances==0):
    print("*******Statistics:*********")
    print("Total games: ",totalgames)
    print("Computer wins total ",computer_win, " games")
    print("Player wins total ",player_win, " games")
