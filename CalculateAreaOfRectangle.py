'''
No-1
Description:-

    Date:-24th Sept 2019.
    Topic:-Basic Python Program.
    Functionality:-To Calculate the Area of a Rectangle. 
    Author:-Kaustubh.S.Bhosekar.
'''

#code

Length=10
Breadth=20
print('Length = ',Length,' Breadth = ',Breadth)
Area=Length*Breadth
print('Area of Rectangle is = ',Area)

'''
Output:-
    
   D:\PYTHON\python programs>python CalculateAreaOfRectangle.py
    Length =  10  Breadth =  20
    Area of Rectangle is =  200

'''

'''
No-2
Description:-

    Date:-24th Sept 2019.
    Topic:-Basic Python Program.
    Functionality:-To Calculate the Area of a Rectangle Optimised Code. 
    Author:-Kaustubh.S.Bhosekar.
'''

#code

Length=10
Breadth=20
print('Length = ',Length,' Breadth = ',Breadth)
print('Area of Rectangle is = ',Length*Breadth)

'''
Output:-
    
   D:\PYTHON\python programs>python CalculateAreaOfRectangle.py
    Length =  10  Breadth =  20
    Area of Rectangle is =  200

'''