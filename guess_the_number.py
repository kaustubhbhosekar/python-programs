'''
Description:-

    Date:-31st Dec 2019.
    Topic:-Basic Python Program.
    Functionality:-To demonstrate the functionality of Random module. 
    Author:-Kaustubh.S.Bhosekar.
'''

#code

import random


golden_number = random.randint(0, 20)
count = int(0)
guess = int(3)

while count != guess:
    choice = int(input("Input a random number from 1 to 20: "))
    if choice == golden_number:
        print("Congratulations, you've found the golden number!")
        break
    elif choice < golden_number:
        count += 1
        print("Your number is too low! You have ", guess-count, "left!")
    elif choice > golden_number:
        count += 1
        print("Your number is too large! You have ", guess-count, "left")


'''
Output:-
   
If success in first attempt:-

    D:\KAUSTUBH DATA\Python\geeks pracs>py guess_the_number.py
    Input a random number from 1 to 20: 4
    Congratulations, you've found the golden number!

D:\KAUSTUBH DATA\Python\geeks pracs>


if not success:-

    D:\KAUSTUBH DATA\Python\geeks pracs>py guess_the_number.py
    Input a random number from 1 to 20: 4
    Your number is too low! You have  2 left!
    Input a random number from 1 to 20: 6
    Your number is too low! You have  1 left!
    Input a random number from 1 to 20: 15
    Your number is too large! You have  0 left

    D:\KAUSTUBH DATA\Python\geeks pracs>

'''

        