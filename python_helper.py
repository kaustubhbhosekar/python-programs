'''
No1
Description:-

    Date:-16th Dec 2019.
    Topic:-To explain use of sqlite.
    Functionality:-To demonstrate the use of sqlite to stode data in database. 
    Author:-Kaustubh.S.Bhosekar.
'''


import sqlite3 as lite
#functionality goes here 

class DatabaseManage(object):

    def __init__(self):
        global con 
        try:
            con = lite.connect('courses.db')
            with con:
                cur = con.cursor()
                sql="CREATE TABLE IF NOT EXISTS course(Id INTEGER PRIMARY KEY AUTOINCREMENT,Name TEXT,Description TEXT,Price TEXT,Is_private BOOLEAN NOT NULL DEFAULT 1)"
                cur.execute(sql)
        except Exception:
            print("Not able to connect to DB!")

    # TODO: Insert Data 
    def insert_data(self,data):
        try:
            with con:
                cur=con.cursor()
                sql="INSERT INTO course(name,description,price,is_private) VALUES(?,?,?,?)"
                cur.execute(sql,data)
                return True
        except Exception:
            return False
    # TODO: Fetch Data
    def fetch_data(self):
        try:
            with con:
                cur=con.cursor()
                sql="select * from course"
                cur.execute(sql)
                return cur.fetchall()
        except Exception:
            return False
    # TODO: Delete Data
    def delete_data(self,id):
        try:
            with con:
                cur=con.cursor()
                sql="delete from course where id=?"
                cur.execute(sql,[id])
                return True
        except Exception:
            return False


# TODO: provide interface to user 

def main():
    print("*"*40)
    print("\n:: COURSE MANAGEMENT :: \n")
    print("*"*40)
    print("\n")
    
    db=DatabaseManage()
    print("#"*40)
    print("\n :: User Manual  :: \n")
    print("#"*40)
    print("\n Press 1. Insert New Course\n")
    print("\n Press 2. Show all Courses\n")
    print("\n Press 3. Delete a  Course (NEED ID OF COURCE)\n")
    print("*"*40)
    print("\n")

    choice=input("\nEnter a choice: ")
    if (choice=="1"):
        name = input("\nEnter Cource name : \n")
        description = input("\nEnter Cource description : \n")
        price = input("\nEnter Cource price : \n")
        private = input("\nIs this cource Private (0/1) : \n")

        if db.insert_data([name,description,price,private]):
            print("cource was inserted successfully")
        else:
            print("unable to insert data")
    elif(choice=="2"):
        print("\n cource list:: \n")
        for index, item in enumerate(db.fetch_data()):
            print("\n Serial No : "+str(index + 1))
            print("Course Id : "+str(item[0]))
            print("Course name : "+str(item[1]))
            print("Course description : "+str(item[2]))
            print("Course price : "+str(item[3]))
            private = 'Yes' if item[4] else 'No'
            print("Is Private"+private)
            print("\n")
    elif(choice=="3"):
        record_id=input("enter the cource ID:")
        if(db.delete_data(record_id)):
            print("course was deleted successfully")
        else:
            print("course deletion un success")
    else:
        print("\nBad choice")

if __name__=='__main__':
    main()




'''
Output:-


D:\PYTHON\python_hitesh>py python_helper.py
****************************************

:: COURSE MANAGEMENT ::

****************************************


########################################

 :: User Manual  ::

########################################

 Press 1. Insert New Course


 Press 2. Show all Courses


 Press 3. Delete a  Course (NEED ID OF COURCE)

****************************************



Enter a choice: 1

Enter Cource name :
Python3

Enter Cource description :
Python3

Enter Cource price :
455

Is this cource Private (0/1) :
1
cource was inserted successfully


again run the code in order to display the cource list

D:\PYTHON\python_hitesh>py python_helper.py
****************************************

:: COURSE MANAGEMENT ::

****************************************


########################################

 :: User Manual  ::

########################################

 Press 1. Insert New Course


 Press 2. Show all Courses


 Press 3. Delete a  Course (NEED ID OF COURCE)

****************************************
Enter a choice: 2


 cource list::


 Serial No : 1
Course Id : 1
Course name : python
Course description : python
Course price : 400
Is PrivateNo



 Serial No : 2
Course Id : 2
Course name : python3
Course description : python3
Course price : 344
Is PrivateYes



 Serial No : 3
Course Id : 4
Course name : Python3
Course description : Python3
Course price : 455
Is PrivateYes


Again run code to delete a specific course


D:\PYTHON\python_hitesh>py python_helper.py
****************************************

:: COURSE MANAGEMENT ::

****************************************


########################################

 :: User Manual  ::

########################################

 Press 1. Insert New Course


 Press 2. Show all Courses


 Press 3. Delete a  Course (NEED ID OF COURCE)

****************************************



Enter a choice: 3
enter the cource ID:3
course was deleted successfully

'''