'''
No1
Description:-

    Date:-25th Sept 2019.
    Topic:-Basic Python Program.
    Functionality:-To demonstrate the use of int and input function. 
    Author:-Kaustubh.S.Bhosekar.
'''

#code

print('Please Enter Number')                       
Num1 = input()                           #Get input from user
print('Num 1 = ',Num1)                   #Print value of Num1
print(type(Num1))                        #Check type of Num1
print('Converting type of Num1 to int ')
Num1=int(Num1)                          #Convert type of Num1 from str to int
print(Num1)                             #print the value of Num1
print(type(Num1))                       #Check type of Num1

'''
Output:-
   
D:\PYTHON\python programs>python toDemonstarteIntandInputFn.py
Please Enter Number
34
Num 1 =  34
<class 'str'>
Converting type of Num1 to int
34
<class 'int'>

'''

'''
No2
Description:-

    Date:-25th Sept 2019.
    Topic:-Basic Python Program.
    Functionality:-To demonstrate the use of int and input function. 
    Author:-Kaustubh.S.Bhosekar.
'''

#code

Num1 = int(input('Please Enter Number: '))
print('Num1 =',Num1)    #print the value of Num1
print(type(Num1))       #check the type of Num1

'''
Output:-
   
D:\PYTHON\python programs>python toDemonstarteIntandInputFn.py
Please Enter Number: 34
Num1 = 34
<class 'int'>

'''