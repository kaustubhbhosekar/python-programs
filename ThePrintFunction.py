'''
No:1:-
Description:-

    Date:-24th Sept 2019.
    Topic:-Basic Python Program.
    Functionality:-Program to display the messages "Hello","World" and "Good Bye".Each of the three messages should get displayed on a different line. 
    Author:-Kaustubh.S.Bhosekar.
'''

#code

print('Hello')
print('World')
print('Good Bye')

'''
Output:-
    
    D:\PYTHON\python programs>python ThePrintFunction.py
    Hello
    World
    Good Bye
'''

'''
No:2:-
Description:-

    Date:-24th Sept 2019.
    Topic:-Basic Python Program.
    Functionality:-Program to display the messages "Hello","World" and "Good Bye".Each of the three messages should get displayed on a single line. 
    Author:-Kaustubh.S.Bhosekar.
'''

#code

print('Hello',end='')
print('World',end='')
print('Good Bye',end='')

'''
Output:-
    
D:\PYTHON\python programs>python ThePrintFunction.py
HelloWorldGood Bye
'''