'''
No-1
Description:-

    Date:-3rd Dec 2019.
    Topic:-Basic Python Program.
    Functionality:-To Demonstrate the use of individual type casting functions. 
    Author:-Kaustubh.S.Bhosekar.

Note:-Type casting is required as the input function in python3 accepts the input in the form of string.    
'''

#code

print('Enter Student Details')
Name=input('Enter Name : ')
Age=input('Enter Age : ')
Gender=input('Enter Gender : ')
Height=input('Enter Height : ')
Eligibility=input('Enter Eligibility True/False : ')

print('-------Data and respective type before type casting-------')

print('Name = ',Name,'type=',type(Name))
print('Age = ',Age,'type=',type(Age))
print('Gender = ',Gender,'type=',type(Gender))
print('Height = ',Height,'type=',type(Height))
print('Eligibility = ',Eligibility,'type=',type(Eligibility))

#perform type casting

Name=str(Name)
Age=int(Age)
Gender=str(Gender)
Height=float(Height)
Eligibility=bool(Eligibility)

print('-------Data and respective type after type casting-------')

print('Name = ',Name,'type=',type(Name))
print('Age = ',Age,'type=',type(Age))
print('Gender = ',Gender,'type=',type(Gender))
print('Height = ',Height,'type=',type(Height))
print('Eligibility = ',Eligibility,'type=',type(Eligibility))

'''
Output:-
    
Enter Student Details
Enter Name : Kaustubh
Enter Age : 26
Enter Gender : Male
Enter Height : 5.6
Enter Eligibility True/False : True
-------Data and respective type before type casting-------
Name =  Kaustubh type= <class 'str'>
Age =  26 type= <class 'str'>
Gender =  Male type= <class 'str'>
Height =  5.6 type= <class 'str'>
Eligibility =  True type= <class 'str'>
-------Data and respective type after type casting-------
Name =  Kaustubh type= <class 'str'>
Age =  26 type= <class 'int'>
Gender =  Male type= <class 'str'>
Height =  5.6 type= <class 'float'>
Eligibility =  True type= <class 'bool'>

'''