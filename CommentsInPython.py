'''
Description:-

    Date:-24th Sept 2019.
    Topic:-Basic Python Program.
    Functionality:-Learn How to Comment in Python. 
    Author:-Kaustubh.S.Bhosekar.
'''

#code

print('I Learnt How to Comment in Python')

'''This is 
Multi Line 
Comment'''

#This is Single Line Comment

print('Bye')


'''
Output:-
    
    D:\PYTHON\python programs>python CommentsInPython.py
    I Learnt How to Comment in Python
    Bye

'''