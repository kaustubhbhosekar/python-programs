'''
No-1
Description:-

    Date:-3rd Dec 2019.
    Topic:-Basic Python Program.
    Functionality:-To Demonstrate the use of int before input function. 
    Author:-Kaustubh.S.Bhosekar.
  
Note:-
        By using this functionality type casting is done at the same time when the input is accepted and then value is assigned to the variable.
'''

#code

Num1=int(input("Enter the number: "))
print('Num1 = ',Num1)
print(type(Num1))

'''
Output:-
    
Enter the number: 67
Num1 =  67
<class 'int'>
'''