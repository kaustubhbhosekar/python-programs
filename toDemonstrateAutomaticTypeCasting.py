'''
No-1
Description:-

    Date:-3rd Dec 2019.
    Topic:-Basic Python Program.
    Functionality:-To Demonstrate the use of Automatic type casting functions. 
    Author:-Kaustubh.S.Bhosekar.

Note:-Type casting is required as the input function in python3 accepts the input in the form of string.

In Order to Simplify Type Casting Python3 provides a function which performs automatic type casting.

eval() function is provided by python to do the same.

******************Important Note***********************************
eval() function is not applicable for data with type str as it is default type and dose not require type casting.
*******************************************************************
'''

#code

print('Enter Student Details')
Name=input('Enter Name : ')
Age=input('Enter Age : ')
Gender=input('Enter Gender : ')
Height=input('Enter Height : ')
Eligibility=input('Enter Eligibility True/False : ')

print('-------Data and respective type before type casting-------')

print('Name = ',Name,'type=',type(Name))
print('Age = ',Age,'type=',type(Age))
print('Gender = ',Gender,'type=',type(Gender))
print('Height = ',Height,'type=',type(Height))
print('Eligibility = ',Eligibility,'type=',type(Eligibility))

#perform type casting

#Name=eval(Name)  returns error
Age=eval(Age)
#Gender=eval(Gender) returns error
Height=eval(Height)
Eligibility=eval(Eligibility)

print('-------Data and respective type after type casting-------')

print('Name = ',Name,'type=',type(Name))
print('Age = ',Age,'type=',type(Age))
print('Gender = ',Gender,'type=',type(Gender))
print('Height = ',Height,'type=',type(Height))
print('Eligibility = ',Eligibility,'type=',type(Eligibility))

'''
Output:-
    
Enter Student Details
Enter Name : Kaustubh
Enter Age : 26
Enter Gender : Male
Enter Height : 5.6
Enter Eligibility True/False : True
-------Data and respective type before type casting-------
Name =  Kaustubh type= <class 'str'>
Age =  26 type= <class 'str'>
Gender =  Male type= <class 'str'>
Height =  5.6 type= <class 'str'>
Eligibility =  True type= <class 'str'>
-------Data and respective type after type casting-------
Name =  Kaustubh type= <class 'str'>
Age =  26 type= <class 'int'>
Gender =  Male type= <class 'str'>
Height =  5.6 type= <class 'float'>
Eligibility =  True type= <class 'bool'>

'''