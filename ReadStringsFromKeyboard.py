'''
Description:-

    Date:-25th Sept 2019.
    Topic:-Basic Python Program.
    Functionality:-To Read strings from keyboard. 
    Author:-Kaustubh.S.Bhosekar.
'''

#code

String1=input('Enter String 1 : ')
String2=input('Enter String 2 : ')

print('String1 :- ',String1)
print('String2 :- ',String2)

'''
Output:-
   
    D:\PYTHON\python programs>python ReadStringsFromKeyboard.py
    Enter String 1 : Hello
    Enter String 2 : Welcome To Python Programming
    String1 :-  Hello
    String2 :-  Welcome To Python Programming

'''