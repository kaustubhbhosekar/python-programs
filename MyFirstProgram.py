'''
Description:-

    Date:-24th Sept 2019.
    Topic:-Basic Python Program.
    Functionality:-To print statements in python. 
    Author:-Kaustubh.S.Bhosekar.
'''

#code

print('Hello Welcome to Python')
print('Awesome Python!')
print('Bye')

'''
Output:-
    
    D:\PYTHON\python programs>python MyFirstProgram.py
    Hello Welcome to Python
    Awesome Python
    Bye

'''